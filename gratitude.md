# Agradecimientos

Este libro no existiría sin las personas que me han acompañado en mi viaje, tanto docentes como colegas. Mi agradecimiento y aprecio a todos mis docentes en mis años de formación por brindarme sus mejores esfuerzos para enseñarme programación en sus diversas formas. Estoy en deuda con todos mis colegas y amigos programadores a lo largo de los años que compartieron sus conocimientos conmigo y confiaron en mí lo suficiente como para ayudarlos en el camino. También tengo la suerte de tener muchas comunidades que me ayudan a mantenerme, incluidas [Michigan!/usr/group][1], [PyOhio][2], [Coffee House Coders][3] y [Ubuntu Michigan Loco][4]. También a los que no encajan en estas categorías. Sepan que si hemos pasado algún tiempo discutiendo la programación u otros asuntos, nuestras discusiones son apreciadas profundamente.

También agradezco el trabajo de Leo Babauta de [Zen Habits][5] que me proporcionó las ideas de _mindfulness_ y contenedores de atención focalizada. Han sido transformadoras en mi propio trabajo, como lo demuestra este libro. Me comprometí a pasar al menos 10 minutos cada mañana escribiendo cada sección, y los resultados son el trabajo que ves ante ti.

Gracias a los que me ayudaron directamente con este proyecto. Gracias a mi madre, Sharon Maloney, por ayudarme en la edición de este libro. Cualquier eerrorr que quede es responsabilidad del autor. Gracias a Beau Sheldon por revisar el capítulo sobre salud mental y por ayudarme a comprenderlo mejor y resaltar las áreas en las que lucha la gente. Gracias a mi amigo, David Revoy, por su increíble portada y por su inspiración durante todo el proyecto. Gracias a Esteban Manchado Velázquez por agregar CSS y limpiar la versión HTML del texto. Gracias a los lectores beta por sus valiosos comentarios y opiniones en repositorio git en Framagit, incluidos (en orden alfabético por identificador o nombre): Brendan Kidwell, D. Joe Anderson, David Revoy, Eric Hallam, Jer Lance, Matthew Piccinato, Matthew Balch, Midgard, Nicholas Guarracino, RJ Quiralta, Valvin y Wilhelm Fitzpatrick. Gracias a Paco Esteban por los arreglos de edición.

Mi más profunda gratitud es para mi mujer JoDee y mis padres por su apoyo y por creer en mí. Las palabras no pueden expresar el amor y el agradecimiento que tengo por ellos.

[1]:http://mug.org
[2]:https://pyohio.org
[3]:http://coffeehousecoders.com
[4]:htttp://loco.ubuntu.com/teams/ubuntu-us-mi
[5]:http://zenhabits.net
