## El programador mediocre

Este es el texto traducido al español del libro "[The Mediocre Programmer](https://themediocreprogrammer.com/)" escrito por Craig Malone.

La traducción al español se ha hecho del libro en inglés, así que se irán incorporando nuevas mejoras o modificaciones según se realicen en la versión original.

Si encuentras un error tipográfico o una errata no dudes en crear un _issue_ o un _pull request_ para corregirla. Si participas en el repositorio aceptas que tus contribuciones se publiquen bajo las mismas condiciones que el repositorio original.

El libro original está publicado bajo una licencia CC-BY-SA 4.0 International. Mi traducción al español también está publicada bajo la misma licencia que el documento original.

Tienes el libro disponible en formato epub, html y pdf, dentro de la carpeta `build` de este repositorio dentro de sendas carpetas para cada formato.

Tienes la traducción disponible en el siguiente enlace:
* [https://victorhck.gitbook.io/el-programador-mediocre/](https://victorhck.gitbook.io/el-programador-mediocre/)
