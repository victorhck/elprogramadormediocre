BUILD = build
BOOKNAME = el_programador_mediocre
TITLE = title.txt
METADATA_XML = metadata.xml
METADATA_YAML = metadata.yaml
CHAPTERS = intro.md chapter01.md chapter02.md chapter03.md chapter04.md chapter05.md chapter06.md chapter07.md epilogue.md gratitude.md appendixa.md
TOC = --toc --toc-depth=2
COVER_IMAGE = images/el_programador_mediocre.png
LATEX_CLASS = book
FONTS = fonts/LinBiolinum_R.ttf fonts/LinBiolinum_RI.ttf fonts/LinBiolinum_RB.ttf

all: book

book: epub html pdf

clean:
	rm -r $(BUILD)

epub: $(BUILD)/epub/$(BOOKNAME).epub

html: $(BUILD)/html/$(BOOKNAME).html fonts css

fonts: $(patsubst %,$(BUILD)/html/%,$(FONTS))

css: $(BUILD)/html/css/$(BOOKNAME).css

pdf: $(BUILD)/pdf/$(BOOKNAME).pdf

$(BUILD)/epub/$(BOOKNAME).epub: $(TITLE) $(CHAPTERS) $(METADATA_YAML)
	mkdir -p $(BUILD)/epub
	pandoc -s $(TOC) --top-level-division=chapter --number-offset=0 --epub-metadata=$(METADATA_XML) --epub-cover-image=$(COVER_IMAGE) -o $@ $^

$(BUILD)/html/$(BOOKNAME).html: $(CHAPTERS) $(METADATA_YAML)
	mkdir -p $(BUILD)/html
	pandoc -s $(TOC) --top-level-division=chapter --number-offset=0 --standalone --to=html5 --css=css/$(BOOKNAME).css -o $@ $^

$(BUILD)/html/fonts/%: fonts/%
	mkdir -p $(BUILD)/html/fonts
	cp $< $@

$(BUILD)/html/css/%: css/%
	mkdir -p $(BUILD)/html/css
	cp $< $@

$(BUILD)/pdf/$(BOOKNAME).pdf: $(CHAPTERS) $(METADATA_YAML)
	mkdir -p $(BUILD)/pdf
	pandoc -s $(TOC) --top-level-division=chapter --number-offset=0 --template=Pandoc/templates/cs-6x9-pdf.latex --pdf-engine=xelatex  -o $@ $^

.PHONY: all book clean epub html fonts css pdf
