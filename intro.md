# Introducción {-}

## ¿El programador mediocre?

Afrontémoslo: no queremos ser programadores mediocres. ¡Queremos ser programadores ``[geniales,increíbles,superlativos]``! Queremos ser los programadores a los que llaman cada vez que están en un aprieto, y queremos ser los programadores que se sumergen en bases de código desconocidas y producen código perfecto en cuestión de minutos. Código que estaría en el Louvre como una obra de arte, estudiado por generaciones de programadores por su belleza intrínseca y funcionalidad excepcional.

¿Por qué íbamos a querer ser programadores mediocres? ¿No es mediocre lo opuesto a genial? ¿No deberíamos esforzarnos por ser grandes programadores?

Por supuesto, deberíamos esforzarnos por ser grandes programadores a largo plazo, pero para llegar a ser grandes programadores primero debemos pasar por la etapa de ser programadores mediocres.

Los programadores mediocres saben que no son (todavía) grandes programadores. Los programadores mediocres ven la distancia entre donde se encuentran actualmente y la grandeza que quieren en sus trabajos de programador. Son conscientes del trabajo que implica ser un gran programador y creen que si hacen el trabajo también llegarán a ser grandes programadores.

Pero también ven sus propias faltas y carencias. Ven el historial de su navegador lleno de búsquedas en internet de sintaxis y conceptos básicos. Ven sus archivos de correo electrónico de preguntas que han hecho a otros programadores. Se estremecen ante su código de hace varios meses y se preguntan si alguna vez llegarán a ser grandes programadores con todos estos errores y traspiés. Ven la brecha entre ellos y los grandes programadores, y se siente como si la brecha se ensanchara a cada paso del camino.

Los programadores mediocres se preguntan si vale la pena. Se preguntan si deberían hacer algo más con sus vidas además de programar computadoras. Tal vez no sean tan buenos como creían, o tal vez les falte ese talento especial que tienen los grandes programadores. Tal vez sientan que aprendieron cosas equivocadas al principio de sus viajes, o tal vez piensen que deberían haber comenzado antes.

Ven que otras personas que tienen un gran éxito como programadores y se preguntan si estuvieron ausentes el día en que se entregaron los grandes genes programadores.

La verdad es que todos somos programadores mediocres de alguna forma. Seguimos realizando preguntas y teniendo que echar un vistazo a sintaxis y conceptos en nuestro día a día cuando programamos. Las computadoras continúan evolucionando y los programadores continúan añadiendo complejidad cada día a las tareas de programación. Conlleva mucho ancho de banda mental mantener todos esos conceptos frescos en nuestra mente.

## ¿Por qué este libro?

Este libro trata de ayudarte en tu viaje como programador mediocre. Juntos descubriremos algunos conceptos erróneos comunes que tenemos sobre la programación, el fracaso y el crecimiento. Entenderemos que el acto de programar y desarrollar es algo que hacemos todos los días. Todos los días podemos mejorar en pequeñas cosas. Son estos pequeños cambios los que nos transforman de programadores mediocres en mejores programadores.

Hay muchos libros sobre cómo convertirse en un mejor programador. Tienden a tener listas de verificación y otros consejos que el autor considera lo suficientemente importantes como para que los hagas a fin de convertirte en un mejor programador. Suelen centrarse en mejoras específicas, como por ejemplo elegir el mejor editor, escribir mejores casos de prueba o beber mucha agua. Esos libros tienen muchos consejos útiles, pero se leen como una lista de cosas que debes hacer todas a la vez para tener éxito. Este libro intentará no cargarte con más trabajo (probablemente ya tengas suficiente). Más bien, discutiremos lo que se siente siendo un programador. Hablaremos de las emociones que aparecen mientras programamos, los sentimientos de frustración, culpa, ira e insuficiencia. Trataremos de las dificultades para aprender cosas nuevas y mantener tus habilidades actualizadas. Hablaremos de esos momentos en los que tienes ganas de darte por vencido y alejarte de la informática y si esos sentimientos provienen de un lugar de amor o de una preocupación por no estar al día.

Este libro es un viaje personal para ambos. Es una memoria de mi tiempo como programador y mis sentimientos a lo largo del camino. He pensado muchas veces en rendirme y encontrar una carrera diferente, pero hacer otra cosa que no sea programador de computadoras me asusta aún más. ¿Significa eso que estoy atrapado en un uróboro (_N. del traductor: un símbolo que muestra a un animal con forma de serpiente que engulle su propia cola y que forma un círculo con su cuerpo. El uróboro simboliza el ciclo eterno de las cosas_) perverso de autocompasión y dudas? Difícilmente. Significa que necesito profundizar más para comprender por qué elegí el camino de ser programador y darme cuenta de que me costó mucho llegar aquí y que me llevará mucho más llegar a donde quiero estar. Es un compromiso de ver las cosas como son ahora y seguir adelante desde donde estoy parado.

## Descargo de responsabilidades

No soy doctor ni terapeuta. No tengo cualificación para darte un consejo médico. Soy programador. Toda la información de este libro está dada desde la perspectiva de un programador con dificultades y no debe tomarse como un consejo médico. Si necesitas ayuda de un profesional médico, por favor busca a una persona que pueda ayudarte. (Hay un capítulo entero sobre buscar ayuda de otras personas casi al final de este libro).

Comencemos nuestro viaje averiguando dónde estamos y recordando qué nos llevó a este lugar.

## De esta traducción

No recuerdo cómo conocí el texto de Craig Maloney, quizás dí con él porque le sigo en Mastodon y desde ahí conocí su proyecto. Cuando lo conocí, antes de leerlo, quizás pensé que sería un montón de recursos y consejos técnicos para las nuevas personas que llegaran a esto de la programación, pero me resultó curioso el título.

Después de leerlo, me dí cuenta que no. Que no se hablaba de nada técnico sobre programación, que lo que aquí se cuenta tiene que ver más con el aspecto psicológico y mental en lo referente a la programación que con aspectos técnicos.

Yo, sin ser programador, encontré en el libro algunos consejos y trucos interesantes para aplicar en mi día a día en algunos aspectos de mi faceta digital (mi blog, traducciones, colaboraciones, etc).

Y como siempre, de ese interés personal nació esta traducción que hoy tienes delante, después de pedir el correspondiente permiso al autor original. Me gustó lo que se contaba y la traducción "me obligaba" a tener que leer el texto y comprenderlo para realizarla y poder compartirla con todo el mundo.

Si tu trabajo tiene que ver de alguna manera con la programación y en algún momento te has sentido una persona perdida, frustrada o fuera de lugar, este texto te ayudará a saber que es algo común y el autor compartirá contigo sus visiones de su propio viaje en el mundo de la programación y cómo consiguió ir pasando etapas en ese viaje. Tu tendrás tu propio camino en el viaje de la programación, pero tomar como referencia lo que otras personas han sentido quizás te sirva como ayuda en los momentos bajos del camino, espero que sea así.

Pero si no eres programador, también es un texto interesante, este es un relato en el que el autor revela sus puntos flacos, y comparte algunos de los recursos que le han ayudado a conseguir sus metas. Quizás tu puedas aplicarlas a tu día a día en otras facetas, a mí me han servido.

La traducción la he realizado en un repositorio git hospedado en [Codeberg](https://codeberg.org/victorhck/elprogramadormediocre) donde tienes disponible todos los capítulos y el código original que utiliza el autor para generar su página web.

Si este u otro contenido que creo te resulta interesante, siempre puedes agradecérmelo mediante una donación en [Liberapay](https://es.liberapay.com/victorhck/). Pero tanto si donas como si no, todo el contenido que creo estará libre para que lo disfrutes.
